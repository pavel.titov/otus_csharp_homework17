﻿using DocumentsReceiver;
using System;
using System.Threading;

namespace Homework17
{
    class Program
    {
        private static ManualResetEventSlim _mres = new ManualResetEventSlim(false);

        static void Main(string[] args)
        {
            var dr = new Receiver();
            dr.DocumentsReady += OnDocumentsReady;
            dr.TimedOut += OnTimedOut;

            dr.Start(@"c:\temp", 1_000);
            dr.Unsubscribe();

            dr.Start(@"c:\temp", 30_000);
            _mres.Wait();
            dr.Unsubscribe();

            Console.ReadKey();
        }

        private static void OnTimedOut()
        {
            Console.WriteLine("Timed out");
            _mres.Set();
        }

        private static void OnDocumentsReady()
        {
            Console.WriteLine("Documents ready");
            _mres.Set();
        }
    }
}
