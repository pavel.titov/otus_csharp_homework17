﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Timers;

namespace DocumentsReceiver
{
    public class Receiver
    {
        public event Action DocumentsReady;
        public event Action TimedOut;

        private FileSystemWatcher _watcher;
        private Timer _timer;
        private readonly Dictionary<string, bool> _filesUploaded;

        public Receiver()
        {
            _filesUploaded = new Dictionary<string, bool>()
            {
                ["Паспорт.jpg"] = false,
                ["Заявление.txt"] = false,
                ["Фото.jpg"] = false,
            };
        }

        public void Start(string targetDirectory, int waitingInterval)
        {
            foreach (var filename in _filesUploaded.Keys)
            {
                _filesUploaded[filename] = false;
            }

            _watcher = new FileSystemWatcher();
            _watcher.Path = targetDirectory;
            _watcher.Created += OnFileCreated;
            _watcher.EnableRaisingEvents = true;

            _timer = new Timer();
            _timer.Interval = waitingInterval;
            _timer.Elapsed += OnTimerElapsed;

            _timer.Start();
        }

        public void Unsubscribe()
        {
            _watcher.Created -= OnFileCreated;
            _timer.Elapsed -= OnTimerElapsed;
            _timer.Dispose();
            _watcher.Dispose();
        }

        private void AddFile(string name)
        {
            if (_filesUploaded.ContainsKey(name))
            {
                _filesUploaded[name] = true;
            }
        }

        private void OnFileCreated(object sender, FileSystemEventArgs e)
        {
            AddFile(e.Name);

            if (_filesUploaded.All(f => f.Value))
            {
                DocumentsReady.Invoke();
            }
        }


        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            TimedOut.Invoke();
            Unsubscribe();
        }
    }
}
